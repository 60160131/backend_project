const Establishment = require('../models/Establishment')
const establishmentController = {
  establishmentList: [],
  addEstablishment (req, res, next) {
    const payload = req.body
    Establishment.create(payload).then(function (establishments) {
      res.json(establishments)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  async updateEstablishment(req, res, next) {
    const payload = req.body
    try {
      const establishments = await Establishment.updateOne({ _id: payload._id }, payload)
      res.json(establishments)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteEstablishment (req, res, next) {
    const { id } = req.params
    try {
      const establishments = await Establishment.deleteOne({ _id: id })
      res.json(establishments)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getEstablishments (req, res, next) {
    try {
      const establishments = await Establishment.find({})
      res.json(establishments)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getEstablishment(req, res, next) {
    const { id } = req.params
    try {
      const establishment = await Establishment.findById(id)
      res.json(establishment)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = establishmentController
