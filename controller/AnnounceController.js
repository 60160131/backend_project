const User = require('../models/Announce')
const announceController = {
    announceList: [],
      async getAnnounces (req, res, next){
        try {
          const announces = await Announce.find({})
          res.json(announces)
        } catch (err) {
          res.status(500).send(err)
        }
      },
      async getAnnounce(req, res, next) {
        const { id } = req.params
        try {
          const announces = await Announce.findById(id)
          res.json(announces)
        } catch (err) {
          res.status(500).send(err)
        }
      }, 
      addAnnounce (req, res, next) {
        const payload = req.body
        Announce.create(payload).then(function (announces) {
          res.json(announces)
        }).catch(function (err) {
          res.status(500).send(err)
        })
      },
      async updateAnnounce(req, res, next) {
        const payload = req.body
        try {
          const announces = await Announce.updateOne({ _id: payload._id }, payload)
          res.json(announces)
        } catch (err) {
          res.status(500).send(err)
        }
      },
      async deleteAnnounce(req, res, next) {
        const { id } = req.params
        try {
          const announces = await Announce.deleteOne({ _id: id })
          res.json(announces)
        } catch (err) {
          res.status(500).send(err)
        }
      }
}
module.exports = announceController