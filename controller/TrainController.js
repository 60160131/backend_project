const Train = require('../models/Train')
const trainController = {
  trainList: [],
  addTrain (req, res, next) {
    const payload = req.body
    Train.create(payload).then(function (trains) {
      res.json(trains)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  async updateTrain (req, res, next) {
    const payload = req.body
    try {
      const trains = await Train.updateOne({ _id: payload._id }, payload)
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteTrain (req, res, next) {
    const { id } = req.params
    try {
      const trains = await Train.deleteOne({ _id: id })
      res.json(trains)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  // async regisTrain (req, res, next) {
  //   const payload = req.body
  //   try {
  //     const trains = await Train.updateOne({ _id: payload._id }, payload)
  //     res.json(trains)
  //   } catch (err) {
  //     res.status(500).send(err)
  //   }
  // },
  getTrains (req, res, next) {
    Train.find({}).then(function (trains) {
      console.log(trains)
      res.json(trains)
    }).catch(function (err) {
      console.log(err)
      res.status(500).send(err)
    })
  },
  getTrain (req, res, next) {
    const { id } = req.params
    Train.findById(id).then(function (train) {
      res.json(train)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = trainController