const mongoose = require('mongoose')
const hourSchema = new mongoose.Schema({
  idPerson: {
    type: String,
    required: true,
    minlength: 8,
    maxlength: 8
  },
  name: {
    type: String,
    required: true,
    minlength: 2,
    unique: true
  },
  branch: {
    type: String,
    required: true,
    minlength: 2,
    unique: true
  },
  ready: {
    type: Number,
    required: true,
    min: 1,
    max: 99
  },
  academic: {
    type: Number,
    required: true,
    min: 1,
    max: 99
  }
})

module.exports = mongoose.model('Hour', hourSchema)