const Professor = require('../models/Professor')
const professorController = {
  professorLists: [],
  lastId: 3,
  async updateProfessor(req, res, next) {
    const payload = req.body
    try {
      const professor = await Professor.updateOne({ _id: payload._id }, payload)
      res.json(professor)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProfessors(req, res, next) {
    try {
      const professors = await Professor.find({})
      res.json(professors)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getProfessor(req, res, next) {
    const { id } = req.params
    try {
      const professor = await Professor.findById(id)
      res.json(professor)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = professorController
