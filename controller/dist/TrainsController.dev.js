"use strict";

var Train = require('../models/Train');

var trainController = {
  trainList: [],
  getTrains: function getTrains(req, res, next) {
    Train.find({}).then(function (trains) {
      console.log(trains);
      res.json(trains);
    })["catch"](function (err) {
      console.log(err);
      res.status(500).send(err);
    });
  },
  getTrain: function getTrain(req, res, next) {
    var id = req.params.id;
    Train.findById(id).then(function (trains) {
      res.json(trains);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  }
};
module.exports = trainController;