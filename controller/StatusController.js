const Status = require('../models/Status')
const statusController = {
  statusLists: [],
  async getStatuss(req, res, next) {
    try {
      const statuss = await Status.find({})
      res.json(statuss)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = statusController