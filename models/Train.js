const mongoose = require('mongoose')
const trainSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  date: {
    type: Date,
    required: true
  },
  time: {
    type: String,
    required: true
  },
  room: {
    type: String,
    required: true,
    unique: true
  },
  type: {
    type: String,
    enum: ['วิชาการ', 'เตรียมความพร้อม'],
    required: true
  },
  corperation: {
    type: String,
    required: true,
    minlength: 3
  },
  hours: {
    type: Number,
    required: true,
    min: 1,
    max: 24
  }
})

module.exports = mongoose.model('Train', trainSchema)