const express = require('express')
const router = express.Router()
const announcesController = require('../controller/AnnounceController')

/* GET users listing. */
router.get('/', announcesController.getAnnounces)

router.get('/:id', announcesController.getAnnounce)

router.post('/', announcesController.addAnnounce)

router.put('/', announcesController.updateAnnounce)

router.delete('/:id', announcesController.deleteAnnounce)

module.exports = router
