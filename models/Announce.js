const mongoose = require('mongoose')
const Schema = mongoose.Schema
const userSchema = new Schema({
  topic: String,
  content: String,
  date: { type: Date, required: true, default: Date.now }
})

module.exports = mongoose.model('Announce', userSchema)
