const Hour = require('../models/Hour')
const hourController = {
  hourList: [
  //   {
  //     id: 1,
  //     idPerson: '59160020',
  //     name: 'นายประจักร ขมักขมิ่น',
  //     branch: 'วิทยาการคอมพิวเตอร์',
  //     ready: '21',
  //     academic: '8'
  //   },
  //   {
  //     id: 2,
  //     idPerson: '59160123',
  //     name: 'นายสมชาย แมนมาก',
  //     branch: 'วิทยาการคอมพิวเตอร์',
  //     date: '16 กุมภาพันธ์  พ.ศ.2562',
  //     ready: '30',
  //     academic: '15'
  //   }
  ],
    addHour (req, res, next) {
    const payload = req.body
    Hour.create(payload).then(function (hours) {
      res.json(hours)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  // async addUser(req, res, next) {
  //   const payload = req.body
  //   const user = new User(payload)
  //   try {
  //     const newuser = await user.save()
  //     res.json(newuser)
  //   } catch (err) {
  //     res.status(500).send(err)
  //   }
  // },
  async updateHour(req, res, next) {
    const payload = req.body
    // res.json(usersController.updateUser(payload))
    try {
      const hour = await Hour.updateOne({ _id: payload._id }, payload)
      res.json(hour)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async deleteHour(req, res, next) {
    const { id } = req.params
    // res.json(usersController.deleteUser(id))
    try {
      const hour = await Hour.deleteOne({ _id: id })
      res.json(hour)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getHours(req, res, next) {
    try {
      const hours = await Hour.find({})
      res.json(hours)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  async getHour(req, res, next) {
    const { id } = req.params
    try {
      const hour = await Hour.findById(id)
      res.json(hour)
    } catch (err) {
      res.status(500).send(err)
    }
  }
}

module.exports = hourController
