const express = require('express');
const router = express.Router();
const hoursController = require('../controller/HourController')

/* GET users listing. */
router.get('/', hoursController.getHours)

router.get('/:id', hoursController.getHour)

router.post('/', hoursController.addHour)

router.put('/', hoursController.updateHour)

router.delete('/:id', hoursController.deleteHour)

module.exports = router;
