"use strict";

var Establishment = require('../models/Establishment');

var establishmentsController = {
  addEstablishment: function addEstablishment(req, res, next) {
    var payload = req.body;
    Establishment.create(payload).then(function (establishments) {
      res.json(establishments);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  updateEstablishment: function updateEstablishment(req, res, next) {
    var payload = req.body; // res.json(studentsController.updateStudent(payload))

    Establishment.updateOne({
      _id: payload._id
    }, payload).then(function (establishments) {
      res.json(establishments);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  },
  deleteEstablishment: function deleteEstablishment(req, res, next) {
    var id, establishments;
    return regeneratorRuntime.async(function deleteEstablishment$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            id = req.params.id;
            _context.prev = 1;
            _context.next = 4;
            return regeneratorRuntime.awrap(Student.deleteOne({
              _id: id
            }));

          case 4:
            establishments = _context.sent;
            res.json(establishments);
            _context.next = 11;
            break;

          case 8:
            _context.prev = 8;
            _context.t0 = _context["catch"](1);
            res.status(500).send(_context.t0);

          case 11:
          case "end":
            return _context.stop();
        }
      }
    }, null, null, [[1, 8]]);
  },
  getEstablishments: function getEstablishments(req, res, next) {
    Establishment.find({}).then(function (establishments) {
      console.log(establishments);
      res.json(establishments);
    })["catch"](function (err) {
      console.log(err);
      res.status(500).send(err);
    });
  },
  getEstablishment: function getEstablishment(req, res, next) {
    var id = req.params.id;
    Establishment.findById(id).then(function (establishments) {
      res.json(establishments);
    })["catch"](function (err) {
      res.status(500).send(err);
    });
  }
};
module.exports = establishmentsController;