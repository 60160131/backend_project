var express = require('express')
var router = express.Router()
const trainsController = require('../controller/TrainController')

/* GET users listing. */
router.get('/', trainsController.getTrains)

router.get('/:id', trainsController.getTrain)

router.post('/', trainsController.addTrain)

router.put('/', trainsController.updateTrain)

router.delete('/:id', trainsController.deleteTrain)

module.exports = router;