const Student = require('../models/Student')
const studentsController = {
  addStudent (req, res, next) {
    const payload = req.body
    Student.create(payload).then(function (students) {
      res.json(students)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  updateStudent (req, res, next) {
    const payload = req.body
    // res.json(studentsController.updateStudent(payload))
    Student.updateOne({ _id: payload._id }, payload).then(function (students) {
      res.json(students)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  },
  async deleteStudent (req, res, next) {
    const { id } = req.params
    try {
      const students = await Student.deleteOne({ _id: id })
      res.json(students)
    } catch (err) {
      res.status(500).send(err)
    }
  },
  getStudents (req, res, next) {
    Student.find({}).then(function (students) {
      console.log(students)
      res.json(students)
    }).catch(function (err) {
      console.log(err)
      res.status(500).send(err)
    })
  },
  getStudent (req, res, next) {
    const { id } = req.params
    Student.findById(id).then(function (students) {
      res.json(students)
    }).catch(function (err) {
      res.status(500).send(err)
    })
  }
}

module.exports = studentsController
