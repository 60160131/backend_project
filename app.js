var express = require('express')
var cors = require('cors')
var path = require('path')
var cookieParser = require('cookie-parser')
var logger = require('morgan')

var indexRouter = require('./routes/index')
var trainsRouter = require('./routes/trains')
var hoursRouter = require('./routes/hours')
var studentsRouter = require('./routes/students')
var establishmentsRouter = require('./routes/establishments')
var announcesRouter = require('./routes/announces')
var statusRouter = require('./routes/status')
var professorRouter = require('./routes/professor')


const mongoose = require('mongoose')

mongoose.connect('mongodb://admin:password@localhost/mydb', { useNewUrlParser: true, useUnifiedTopology: true })
// mongoose.connect('mongodb://admin:password@localhost/mydb', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// })

const db = mongoose.connection

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', function () {
  console.log('connect')
})

var app = express()

app.use(logger('dev'))
app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use(express.static(path.join(__dirname, 'public')))
app.use(cors())

app.use('/', indexRouter)
app.use('/trains', trainsRouter)
app.use('/hours', hoursRouter)
app.use('/establishments', establishmentsRouter)
app.use('/students', studentsRouter)
app.use('/announces', announcesRouter)
app.use('/status', statusRouter)
app.use('/professors', professorRouter)
module.exports = app
