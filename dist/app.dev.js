"use strict";

var express = require('express');

var cors = require('cors');

var path = require('path');

var cookieParser = require('cookie-parser');
var logger = require('morgan');
var indexRouter = require('./routes/index');
var trainsRouter = require('./routes/trains')
var establishmentsRouter = require('./routes/establishments')

var mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/mydb', {
  useNewUrlParser: true,
  useUnifiedTopology: true
}); // mongoose.connect('mongodb://admin:password@localhost/mydb', {
//   useNewUrlParser: true,
//   useUnifiedTopology: true
// })

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', function () {
  console.log('connect');
});
var app = express();
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(cors());
app.use('/', indexRouter);
app.use('/trains', trainsRouter)
app.use('/establishments', establishmentsRouter)
module.exports = app;