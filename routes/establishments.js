var express = require('express')
var router = express.Router()
const establishmentsController = require('../controller/EstablishmentController')

/* GET users listing. */
router.get('/', establishmentsController.getEstablishments)

router.get('/:id', establishmentsController.getEstablishment)

router.post('/', establishmentsController.addEstablishment)

router.put('/', establishmentsController.updateEstablishment)

router.delete('/:id', establishmentsController.deleteEstablishment)

module.exports = router;
