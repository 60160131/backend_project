const mongoose = require('mongoose')
const establishmentSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    minlength: 3,
    unique: true
  },
  position: {
    type: String,
    required: true
  },
  acceptee: {
    type: Number,
    required: true,
  },
  history: {
    type: String,
    required: true,
  },
  salary: {
    type: Number,
    required: true,
  },
  email: {
    type: String,
    required: true
  }
})

module.exports = mongoose.model('Establishment', establishmentSchema)